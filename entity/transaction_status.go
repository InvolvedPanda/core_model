package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/pechenini/errors"
)

const (
	STATUS_CREATED        = "created"
	STATUS_SIGNED         = "signed"
	STATUS_REVIEW         = "review"
	STATUS_READY_TO_CLEAR = "ready_to_clear"
	STATUS_SENT_TO_PACK   = "sent_to_pack"
	STATUS_PACKED         = "packed"
	STATUS_SENT_TO_CLEAR  = "sent_to_clear"
	STATUS_COMPLETED      = "completed"
	STATUS_SOME_PROBLEMS  = "some_problems"
	STATUS_CANCELLED      = "cancelled"
	STATUS_REJECTED       = "rejected"
	STATUS_INVALID        = "invalid"
	STATUS_ACCEPTED       = "accepted"
	STATUS_TO_SIGN        = "to_sign"
	STATUS_RESERVED       = "reserved"
	STATUS_TO_RESERVE     = "to_reserve"
	STATUS_RECEIVED       = "received"
	STATUS_DROPPED        = "dropped"
)

type TransactionStatus struct {
	Id             int64          `db:"id"`
	Name           string         `db:"name"`
	Hardvalue      string         `db:"hardvalue"`
	Action         sql.NullString `db:"action"`
	NameClientUI   string         `db:"name_client_ui"`
	ClientStatusId sql.NullInt64  `db:"client_status_id"`
}

func (t TransactionStatus) IsInPack() bool {
	return contains([]string{
		STATUS_SENT_TO_PACK,
		STATUS_PACKED,
	}, t.Hardvalue)
}

func (t TransactionStatus) IsUnprocessable() bool {
	return contains([]string{
		STATUS_CANCELLED,
		STATUS_REJECTED,
		STATUS_SOME_PROBLEMS,
		STATUS_RECEIVED,
		STATUS_DROPPED,
	}, t.Hardvalue)
}

func (t TransactionStatus) IsBeforeSentToClear() bool {
	return contains([]string{
		STATUS_CREATED,
		STATUS_TO_SIGN,
		STATUS_SIGNED,
		STATUS_RESERVED,
		STATUS_REVIEW,
		STATUS_TO_RESERVE,
	}, t.Hardvalue)
}

func (t TransactionStatus) IsBeforeReserve() bool {
	return contains([]string{
		STATUS_CREATED,
		STATUS_TO_SIGN,
		STATUS_SIGNED,
		STATUS_TO_RESERVE,
	}, t.Hardvalue)
}

func (t TransactionStatus) IsBeforePacked() bool {
	return contains([]string{
		STATUS_CREATED,
		STATUS_TO_SIGN,
		STATUS_SIGNED,
		STATUS_REVIEW,
		STATUS_TO_RESERVE,
		STATUS_RESERVED,
		STATUS_READY_TO_CLEAR,
	}, t.Hardvalue)
}

func (t TransactionStatus) IsNotSentYet() bool {
	return contains([]string{
		STATUS_CREATED,
		STATUS_TO_SIGN,
		STATUS_SIGNED,
		STATUS_REVIEW,
		STATUS_TO_RESERVE,
		STATUS_RESERVED,
		STATUS_READY_TO_CLEAR,
		STATUS_SENT_TO_PACK,
		STATUS_PACKED,
	}, t.Hardvalue)
}

func (t TransactionStatus) IsMoneyReleased() bool {
	return contains([]string{
		STATUS_SENT_TO_CLEAR,
		STATUS_REJECTED,
		STATUS_ACCEPTED,
		STATUS_COMPLETED,
	}, t.Hardvalue)
}

type TransactionStatusRepository struct {
	DB *sqlx.DB
	BasicRepository
	HardvalueFinder
}

func NewTransactionStatusRepository(db *sqlx.DB) TransactionStatusRepository {
	return TransactionStatusRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "transaction_statuses",
		},
		HardvalueFinder{
			DB:        db,
			TableName: "transaction_statuses",
		}}
}

func (repo TransactionStatusRepository) GetTrxStatusMap() (map[string]TransactionStatus, error) {
	var statusMap = make(map[string]TransactionStatus)
	var statuses []TransactionStatus
	err := repo.GetAll(&statuses)
	if err != nil {
		return statusMap, errors.Wrap(err)
	}
	for _, status := range statuses {
		statusMap[status.Hardvalue] = status
	}

	return statusMap, nil
}

func (repo TransactionStatusRepository) GetTrxStatusIdMap() (map[int64]TransactionStatus, error) {
	var statusMap = make(map[int64]TransactionStatus)
	var statuses []TransactionStatus
	err := repo.GetAll(&statuses)
	if err != nil {
		return statusMap, errors.Wrap(err)
	}
	for _, status := range statuses {
		statusMap[status.Id] = status
	}

	return statusMap, nil
}
