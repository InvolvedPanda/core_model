package entity

import (
	"database/sql"
	errorcreator "errors"
	"github.com/jmoiron/sqlx"
	"github.com/pechenini/errors"
)

type TransactionOutbound struct {
	Id int64 `db:"id"`

	MethodId   sql.NullInt64  `db:"method_id"`
	SepaMsgId  sql.NullInt64  `db:"sepa_msg_id"`
	EndToEndId sql.NullString `db:"end_to_end_id"`
	SepaTrxId  sql.NullString `db:"sepa_trx_id"`

	DrAcc sql.NullString `db:"dr_acc"`
	CrAcc sql.NullString `db:"cr_acc"`

	DrCodeCompany          sql.NullString `db:"dr_code_company"`
	DrAltCodeCompany       sql.NullString `db:"dr_alt_code_company"`
	DrAltCodeCompanyIssuer sql.NullString `db:"dr_alt_code_company_issuer"`
	DrCodePerson           sql.NullString `db:"dr_code_person"`
	DrCodeIssuer           sql.NullString `db:"dr_code_issuer"`

	CrCodeCompany          sql.NullString `db:"cr_code_company"`
	CrAltCodeCompany       sql.NullString `db:"cr_alt_code_company"`
	CrAltCodeCompanyIssuer sql.NullString `db:"cr_alt_code_company_issuer"`
	CrCodePerson           sql.NullString `db:"cr_code_person"`
	CrCodeIssuer           sql.NullString `db:"cr_code_issuer"`

	DrBankBic sql.NullString `db:"dr_bank_bic"`
	CrBankBic sql.NullString `db:"cr_bank_bic"`

	Purpose                 sql.NullString `db:"trx_purpose"`
	PurposeInformation      sql.NullString `db:"trx_purpose_information"`
	PurposeStructuredRef    sql.NullString `db:"trx_purpose_structured_ref"`
	PurposeStructuredIssuer sql.NullString `db:"trx_purpose_structured_issuer"`

	DrBank5Digit sql.NullString `db:"dr_bank_5digit"`
	CrBank5Digit sql.NullString `db:"cr_bank_5digit"`

	InstructionId sql.NullString `db:"instruction_id"`
	Status        sql.NullString `db:"status"`
	StatusReason  sql.NullString `db:"status_reason"`

	SettlementDate sql.NullTime `db:"settlement_date"`

	Udr
	PaymentId sql.NullString `db:"payment_id"`
	Ucr

	DrSchemeCode        sql.NullString `db:"dr_scheme_code"`
	DrSchemeProprietary sql.NullString `db:"dr_scheme_proprietary"`
	DrIdType            sql.NullInt64  `db:"dr_id_type"`
	CrSchemeCode        sql.NullString `db:"cr_scheme_code"`
	CrSchemeProprietary sql.NullString `db:"cr_scheme_proprietary"`
	CrIdType            sql.NullInt64  `db:"cr_id_type"`

	DrName           string         `db:"dr_name"`
	DrAddress        sql.NullString `db:"dr_address"`
	DrAddressCity    sql.NullString `db:"dr_address_city"`
	DrAddressCountry sql.NullString `db:"dr_address_country"`

	CrName           string         `db:"cr_name"`
	CrAddress        sql.NullString `db:"cr_address"`
	CrAddressCity    sql.NullString `db:"cr_address_city"`
	CrAddressCountry sql.NullString `db:"cr_address_country"`

	DrAmount int64 `db:"dr_amount"`

	DrCcyIsocode sql.NullString `db:"dr_ccy_isocode"`

	DrAccountNumber sql.NullString `db:"dr_account_number"`
	DrSortCode      sql.NullString `db:"dr_sort_code"`

	CrAccountNumber sql.NullString `db:"cr_account_number"`
	CrSortCode      sql.NullString `db:"cr_sort_code"`

	CrctedCrSortCode      sql.NullString `db:"crcted_cr_sort_code"`
	CrctedCrAccountNumber sql.NullString `db:"crcted_cr_account_number"`

	Fpid sql.NullString `db:"fpid"`
}

func (to TransactionOutbound) getMethod(db *sqlx.DB) (TransactionMethod, error) {
	var method TransactionMethod
	if !to.MethodId.Valid {
		return method, errorcreator.New("transaction method is null")
	}
	err := NewTransactionMethodRepository(db).GetById(to.MethodId.Int64, &method)
	return method, errors.Wrap(err)
}


type TransactionOutboundRepository struct {
	DB *sqlx.DB
	BasicRepository
}

func NewTransactionOutboundRepository(db *sqlx.DB) TransactionOutboundRepository {
	return TransactionOutboundRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "trx_outbound",
		},
	}
}
