package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/pechenini/errors"
)

const (
	TYPE_CCY_EXCHANGE               = "ccy_exchange"
	TYPE_FEE                        = "fee"
	TYPE_PAYMENT                    = "payment"
	TYPE_PAYMENT_CANCELLATION       = "payment_cancellation"
	TYPE_PAYMENT_RETURN             = "payment_return"
	TYPE_FEE_CANCELLATION           = "fee_cancellation"
	TYPE_ADJUSTMENT                 = "adjustment"
	TYPE_PAYMENT_TECHNICAL_REVERSAL = "payment_technical_reversal"
	TYPE_PAYMENT_REVERSAL           = "payment_reversal"
)

type TransactionType struct {
	Id        int64          `db:"id"`
	Name      string         `db:"name"`
	Hardvalue string         `db:"hardvalue"`
	Comment   sql.NullString `db:"comment"`
	Timestamps
}

func (t TransactionType) IsPayment() bool {
	return t.Hardvalue == TYPE_PAYMENT
}

func (t TransactionType) IsExchange() bool {
	return t.Hardvalue == TYPE_CCY_EXCHANGE
}

func (t TransactionType) IsFee() bool {
	return t.Hardvalue == TYPE_FEE
}

func (t TransactionType) IsPaymentCancellation() bool {
	return t.Hardvalue == TYPE_PAYMENT_CANCELLATION
}

func (t TransactionType) IsPaymentReturn() bool {
	return t.Hardvalue == TYPE_PAYMENT_RETURN
}

func (t TransactionType) IsPaymentReversal() bool {
	return t.Hardvalue == TYPE_PAYMENT_REVERSAL
}

func (t TransactionType) IsFeeCancellation() bool {
	return t.Hardvalue == TYPE_FEE_CANCELLATION
}

func (t TransactionType) IsAdjustment() bool {
	return t.Hardvalue == TYPE_ADJUSTMENT
}

func (t TransactionType) IsPaymentTechReversal() bool {
	return t.Hardvalue == TYPE_PAYMENT_TECHNICAL_REVERSAL
}

type TransactionTypeRepository struct {
	DB *sqlx.DB
	BasicRepository
	HardvalueFinder
}

func NewTransactionTypeRepository(db *sqlx.DB) TransactionTypeRepository {
	return TransactionTypeRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "transaction_types",
		},
		HardvalueFinder{
			DB:        db,
			TableName: "transaction_types",
		}}
}

func (repo TransactionTypeRepository) GetTrxTypeMap() (map[string]TransactionType, error) {
	var typeMap = make(map[string]TransactionType)
	var types []TransactionType
	err := repo.GetAll(&types)
	if err != nil {
		return typeMap, errors.Wrap(err)
	}
	for _, status := range types {
		typeMap[status.Hardvalue] = status
	}

	return typeMap, nil
}

func (repo TransactionTypeRepository) GetTrxTypeIdMap() (map[int64]TransactionType, error) {
	var typeMap = make(map[int64]TransactionType)
	var types []TransactionType
	err := repo.GetAll(&types)
	if err != nil {
		return typeMap, errors.Wrap(err)
	}
	for _, status := range types {
		typeMap[status.Id] = status
	}

	return typeMap, nil
}
