package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type Product struct {
	Id                  int64          `db:"id"`
	ProductActionId     int64          `db:"product_action_id"`
	Priority            int64          `db:"priority"`
	Name                string         `db:"name"`
	Comment             sql.NullString `db:"comment"`
	ValidFromDate       sql.NullTime   `db:"valid_from_date"`
	ValidToDate         sql.NullTime   `db:"valid_to_date"`
	PriceType           string         `db:"price_type"`
	Price               int64          `db:"price"`
	MinPriceAbsolute    sql.NullInt64  `db:"min_price_absolute"`
	MaxPriceAbsolute    sql.NullInt64  `db:"max_price_absolute"`
	Deleted             sql.NullInt64  `db:"deleted"`
	PriceName           string         `db:"price_name"`
	IsDefault           sql.NullBool   `db:"is_default"`
	CcyIsocode          string         `db:"ccy_isocode"`
	FeePaidBy           string         `db:"fee_paid_by"`
	PriceType2nd        sql.NullString `db:"price_type_2nd"`
	Price2nd            sql.NullInt64  `db:"price_2nd"`
	PriceName2nd        sql.NullString `db:"price_name_2nd"`
	MinPriceAbsolute2nd sql.NullInt64  `db:"min_price_absolute_2nd"`
	MaxPriceAbsolute2nd sql.NullInt64  `db:"max_price_absolute_2nd"`
	IsManual            bool           `db:"is_manual"`
	PlanId              sql.NullInt64  `db:"plan_id"`
	Charged             sql.NullString `db:"charged"`
	Calculated          sql.NullString `db:"calculated"`
	Timestamps
}

type ProductRepository struct {
	DB *sqlx.DB
	BasicRepository
}

func NewProductRepository(db *sqlx.DB) ProductRepository {
	return ProductRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "products",
		},
	}
}
