package entity

type TransactionProcessingError struct {
	Value         error
	TransactionId int64
}
