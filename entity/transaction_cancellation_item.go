package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type TransactionCancellationItem struct {
	Id                     int64          `db:"id"`
	CancellationId         int64          `db:"cancellation_id"`
	TrxId                  int64          `db:"trx_id"`
	CancellationSepaId     sql.NullString `db:"cancellation_sepa_id"`
	ReturnSepaId           sql.NullString `db:"return_sepa_id"`
	ResultSepaId           sql.NullString `db:"result_sepa_id"`
	ReportRequestSepaMsgId sql.NullInt64  `db:"report_request_sepa_msg_id"`
	ReportRequestSepaId    sql.NullString `db:"report_request_sepa_id"`
	Timestamps
}

type TransactionCancellationItemRepository struct {
	DB *sqlx.DB
	BasicRepository
}

func NewTransactionCancellationItemRepository(db *sqlx.DB) TransactionCancellationItemRepository {
	return TransactionCancellationItemRepository{
		DB: db,
		BasicRepository: BasicRepository{
			DB:        db,
			TableName: "transaction_cancellation_items",
		},
	}
}
