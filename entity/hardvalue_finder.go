package entity

import (
	"github.com/jmoiron/sqlx"
)

type HardvalueFinder struct {
	DB        *sqlx.DB
	TableName string
}

func (finder HardvalueFinder) FindByHardvalue(hardvalue string, dest interface{}) error {
	return finder.DB.Get(dest, "SELECT * FROM "+finder.TableName+" WHERE hardvalue=$1", hardvalue)
}
