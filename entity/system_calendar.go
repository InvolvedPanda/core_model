package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/pechenini/errors"
	"time"
)

type SystemCalendar struct {
	Id               int64        `db:"id"`
	BusinessDayStart sql.NullTime `db:"business_day_start"`
	BusinessDayEnd   sql.NullTime `db:"business_day_end"`
	Timestamps
}

type SystemCalendarRepository struct {
	DB *sqlx.DB
	BasicRepository
}

func NewSystemCalendarRepository(db *sqlx.DB) SystemCalendarRepository {
	return SystemCalendarRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "system_calendar",
		},
	}
}

func (r SystemCalendarRepository) GetBusinessDay(dateTime time.Time) (*SystemCalendar, error) {
	var days []SystemCalendar
	err := r.DB.Select(&days, "SELECT c.* FROM system_calendar c WHERE business_day_start::DATE = $1", dateTime.Format("2006-01-02"))
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		return nil, errors.Wrap(err)
	}

	return &days[0], errors.Wrap(err)
}

func (r SystemCalendarRepository) GetNextBusinessDay(dateTime time.Time) (*SystemCalendar, error) {
	var days []SystemCalendar
	err := r.DB.Select(&days, "SELECT c.* FROM system_calendar c WHERE business_day_start > $1 ORDER BY business_day_start", dateTime.Format("2006-01-02")+" 23:59:59")
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		return nil, errors.Wrap(err)
	}

	return &days[0], errors.Wrap(err)
}
