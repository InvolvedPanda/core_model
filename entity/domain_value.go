package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/pechenini/errors"
)

const (
	DOMAIN_BANK = "BANK"
)

type DomainValue struct {
	Id            int64          `db:"id"`
	Domain        string         `db:"domain"`
	LowValue      string         `db:"low_value"`
	HighValue     string         `db:"high_value"`
	Comment       sql.NullString `db:"comment"`
	UiEditable    bool           `db:"ui_editable"`
	UiLabel       sql.NullString `db:"ui_label"`
	UiDescription sql.NullString `db:"ui_description"`
}

type DomainValueRepository struct {
	DB *sqlx.DB
	BasicRepository
}

func NewDomainValueRepository(db *sqlx.DB) DomainValueRepository {
	return DomainValueRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "domain_values",
		},
	}
}

func (r DomainValueRepository) FindByLowValue(domain, lowValue string) (DomainValue, error) {
	dv := DomainValue{}
	err := r.DB.Get(&dv, "SELECT * from domain_values WHERE domain=$1 AND low_value=$2", domain, lowValue)
	if err != nil {
		return dv, errors.Wrap(err)
	}
	return dv, nil
}

func (r DomainValueRepository) GetEndOfDay() (DomainValue, error) {
	return r.FindByLowValue(DOMAIN_BANK, "end_of_day")
}
func (r DomainValueRepository) GetEndOfDayStep2() (DomainValue, error) {
	return r.FindByLowValue(DOMAIN_BANK, "end_of_day_step2")
}
