package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

const (
	METHOD_SEPA         = "sepa"
	METHOD_INTERNAL     = "internal"
	METHOD_FPS          = "fps"
	METHOD_SDD          = "sdd"
	METHOD_SDD_INTERNAL = "sdd_internal"
)

type TransactionMethod struct {
	Id        int64          `db:"id"`
	Name      string         `db:"name"`
	Hardvalue string         `db:"hardvalue"`
	Comment   sql.NullString `db:"comment"`
	Timestamps
}

func (t TransactionMethod) IsInternal() bool {
	return t.Hardvalue == METHOD_INTERNAL
}

func (t TransactionMethod) IsFps() bool {
	return t.Hardvalue == METHOD_FPS
}

func (t TransactionMethod) IsSepa() bool {
	return t.Hardvalue == METHOD_SEPA
}

func (t TransactionMethod) IsSDD() bool {
	return t.Hardvalue == METHOD_SDD
}

func (t TransactionMethod) IsSDDInternal() bool {
	return t.Hardvalue == METHOD_SDD_INTERNAL
}

type TransactionMethodRepository struct {
	DB *sqlx.DB
	BasicRepository
	HardvalueFinder
}

func NewTransactionMethodRepository(db *sqlx.DB) TransactionMethodRepository {
	return TransactionMethodRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "transaction_methods",
		},
		HardvalueFinder{
			DB:        db,
			TableName: "transaction_methods",
		}}
}

func (repo TransactionMethodRepository) GetTrxMethodMap() (map[string]TransactionMethod, error) {
	var methodMap = make(map[string]TransactionMethod)
	var methods []TransactionMethod
	err := repo.GetAll(&methods)
	if err != nil {
		return methodMap, err
	}
	for _, status := range methods {
		methodMap[status.Hardvalue] = status
	}

	return methodMap, nil
}

func (repo TransactionMethodRepository) GetTrxMethodIdMap() (map[int64]TransactionMethod, error) {
	var methodMap = make(map[int64]TransactionMethod)
	var methods []TransactionMethod
	err := repo.GetAll(&methods)
	if err != nil {
		return methodMap, err
	}
	for _, status := range methods {
		methodMap[status.Id] = status
	}

	return methodMap, nil
}
