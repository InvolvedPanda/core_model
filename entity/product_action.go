package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

const (
	ACTION_MANUAL_FEE          = "manual_fee"
	ACTION_ACCOUNT_CURRENCY    = "account_currency"
	ACTION_INTERNAL_PAYMENT    = "internal_payment"
	ACTION_SEPA_PAYMENT        = "sepa_payment"
	ACTION_CURRENCY_CONVERSION = "currency_conversion"
	ACTION_MASS_PAYMENTS       = "mass_payments"
	ACTION_PAYMENT_CANCELATION = "payment_cancelation"
	ACTION_BANK_ACCOUNT        = "bank_account"
	ACTION_RECURRING           = "recurring"
	ACTION_FPS_PAYMMENT        = "fps_payment"
)

type ProductAction struct {
	Id        int64          `db:"id"`
	Name      string         `db:"name"`
	Hardvalue string         `db:"hardvalue"`
	Comment   sql.NullString `db:"comment"`
	Deleted   sql.NullBool   `db:"deleted"`
	Timestamps
}

type ProductActionRepository struct {
	DB *sqlx.DB
	BasicRepository
	HardvalueFinder
}

func NewProductActionRepository(db *sqlx.DB) ProductActionRepository {
	return ProductActionRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "product_actions",
		},
		HardvalueFinder{
			DB:        db,
			TableName: "product_actions",
		}}
}
