package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/pechenini/errors"
)

type Bank struct {
	Id            int64          `db:"id"`
	Name          string         `db:"name"`
	Bic           string         `db:"bic"`
	Code          sql.NullString `db:"code"`
	DirectBic     sql.NullString `db:"directbic"`
	SettlementBic sql.NullString `db:"settlementbic"`
	DateFrom      sql.NullTime   `db:"datefrom"`
	DateTo        sql.NullTime   `db:"dateto"`
	Status        int64          `db:"status"`
	Cai           sql.NullString `db:"cai"`
	Type          sql.NullString `db:"type"`
	Lt            sql.NullString `db:"lt"`
	Srbparts      bool           `db:"srbparts"`
	Sdpparts      bool           `db:"sdpparts"`
	AgentType     sql.NullString `db:"agent_type"`
	IsStep2       bool           `db:"is_step2"`
	Timestamps
}

type BankRepository struct {
	DB *sqlx.DB
	BasicRepository
}

func NewBankRepository(db *sqlx.DB) BankRepository {
	return BankRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "banks",
		},
	}
}

func (r BankRepository) GetByBic(bic string) ([]Bank, error) {
	runes := []rune(bic)
	first8Chars := string(runes[0:8])

	var banks []Bank
	err := r.DB.Select(&banks, "SELECT * from banks WHERE bic LIKE '%$1%'", first8Chars)
	return banks, errors.Wrap(err)
}
