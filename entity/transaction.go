package entity

import (
	"database/sql"
	errorcreator "errors"
	"github.com/jmoiron/sqlx"
	"github.com/pechenini/errors"
)

type Transaction struct {
	Id                int64           `db:"id"`
	DrClientId        sql.NullInt64   `db:"dr_client_id"`
	CrClientId        sql.NullInt64   `db:"cr_client_id"`
	DrSubaccId        sql.NullInt64   `db:"dr_subacc_id"`
	CrSubaccId        sql.NullInt64   `db:"cr_subacc_id"`
	DrAccId           sql.NullInt64   `db:"dr_acc_id"`
	CrAccId           sql.NullInt64   `db:"cr_acc_id"`
	TypeId            int64           `db:"type_id"`
	StatusId          int64           `db:"status_id"`
	ProductId         int64           `db:"product_id"`
	ParentTrxId       sql.NullInt64   `db:"parent_trx_id"`
	DrAmount          int64           `db:"dr_amount"`
	CrAmount          int64           `db:"cr_amount"`
	DocId             sql.NullInt64   `db:"doc_id"`
	StartBalance      sql.NullInt64   `db:"start_balance"`
	EndBalance        sql.NullInt64   `db:"end_balance"`
	StatusReason      sql.NullString  `db:"status_reason"`
	DrCcyIsoocode     string          `db:"dr_ccy_isocode"`
	CrCcyIsoocode     string          `db:"cr_ccy_isocode"`
	RingId            sql.NullInt64   `db:"ring_id"`
	TrxInboundId      sql.NullInt64   `db:"trx_inbound_id"`
	TrxOutboundId     sql.NullInt64   `db:"trx_outbound_id"`
	TrxCancellationId sql.NullInt64   `db:"trx_cancellation_id"`
	ExSubAccId        sql.NullInt64   `db:"ex_subacc_id"`
	ExAccId           sql.NullInt64   `db:"ex_acc_id"`
	ExCcyIsoocode     sql.NullString  `db:"ex_ccy_isocode"`
	CcyExchangeRate   sql.NullFloat64 `db:"ccy_exchange_rate"`
	SignatureTotal    float64         `db:"signature_total"`
	UpdatedId         sql.NullInt64   `db:"updated_id"`
	ReviewComment     sql.NullString  `db:"review_comment"`
	DropFees          bool            `db:"drop_fees"`
	StatusUpdatedAt   sql.NullTime    `db:"status_updated_at"`
	OriginalPart      sql.NullString  `db:"originating_party"`
	FileName          sql.NullString  `db:"file_name"`
	Plais             bool            `db:"plais"`
	PlaisMessageId    sql.NullInt64   `db:"plais_message_id"`
	CreatedId         sql.NullInt64   `db:"created_id"`
	ProcessingInfo    sql.NullString  `db:"processing_info"`
	FeeAmount         sql.NullInt64   `db:"fee_amount"`
	FeeAmount2nd      sql.NullInt64   `db:"fee_amount_2nd"`
	FeeAmountCalc     sql.NullInt64   `db:"fee_amount_calc"`

	Timestamps
}

func (t Transaction) IsInbound() bool {
	return t.TrxInboundId.Valid
}

func (t Transaction) IsOutbound() bool {
	return t.TrxOutboundId.Valid
}

func (t Transaction) getOutbound(db *sqlx.DB) (TransactionOutbound, error) {
	var outbound TransactionOutbound
	if !t.IsOutbound() {
		return outbound, errorcreator.New("transaction is not outbound")
	}
	err := NewTransactionOutboundRepository(db).GetById(t.TrxOutboundId.Int64, &outbound)
	return outbound, errors.Wrap(err)
}

func (t Transaction) getInbound(db *sqlx.DB) (TransactionInbound, error) {
	var inbound TransactionInbound
	if !t.IsInbound() {
		return inbound, errorcreator.New("transaction is not inbound")
	}
	err := NewTransactionInboundRepository(db).GetById(t.TrxInboundId.Int64, &inbound)
	return inbound, errors.Wrap(err)
}

type TransactionRepository struct {
	DB *sqlx.DB
	BasicRepository
}

func NewTransactionRepository(db *sqlx.DB) TransactionRepository {
	return TransactionRepository{
		db,
		BasicRepository{
			DB:        db,
			TableName: "transactions",
		},
	}
}
