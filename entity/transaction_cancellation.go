package entity

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

const (
	CANCELLATION_INPROGRESS    = 1;
	CANCELLATION_COMPLETED     = 2;
	CANCELLATION_REJECTED      = 3;
	CANCELLATION_ACCEPTED      = 4;
	REQUEST_DIRECTION_OUTBOUND = 1;
	REQUEST_DIRECTION_INBOUNND = 2;
)

type TransactionCancellation struct {
	Id                 int64          `db:"id"`
	Reason             sql.NullString `db:"reason"`
	ReasonAdditional   sql.NullString `db:"reason_additional"`
	AdditionalComment  sql.NullString `db:"additional_comment"`
	Status             int64          `db:"status"`
	SepaMsgId          sql.NullInt64  `db:"sepa_msg_id"`
	RequestDirection   sql.NullInt64  `db:"request_direction"`
	StatusCode         sql.NullString `db:"status_code"`
	StatusReason       sql.NullString `db:"status_reason"`
	CancellationSepaId sql.NullString `db:"cancellation_sepa_id"`
	OriginatorName     sql.NullString `db:"originator_name"`

	ResultSepaMsgId   sql.NullString `db:"result_sepa_msg_id"`
	ResultReason      sql.NullString `db:"result_reason"`
	ResultAdditional  sql.NullString `db:"result_additional"`
	ResultComment_one sql.NullString `db:"result_comment_one"`
	ResultComment_two sql.NullString `db:"result_comment_two"`
	ResultStatus      sql.NullString `db:"result_status"`
	ResultSepaId      sql.NullString `db:"result_sepa_id"`

	ReturnSepaMsgId      sql.NullString `db:"return_sepa_msg_id"`
	ReturnReason         sql.NullString `db:"return_reason"`
	ReturnAdditionalInfo sql.NullString `db:"return_additional_info"`
	ReturnAmount         sql.NullString `db:"return_amount"`
	ReturnTaxAmount      sql.NullString `db:"return_tax_amount"`
	ReturnSepaId         sql.NullString `db:"return_sepa_id"`

	Timestamps
}

func (t TransactionCancellation) IsInProgress() bool {
	return t.Status == CANCELLATION_INPROGRESS
}

func (t TransactionCancellation) IsCompleted() bool {
	return t.Status == CANCELLATION_COMPLETED
}

func (t TransactionCancellation) IsRejected() bool {
	return t.Status == CANCELLATION_REJECTED
}

func (t TransactionCancellation) IsAccepted() bool {
	return t.Status == CANCELLATION_ACCEPTED
}

type TransactionCancellationRepository struct {
	DB *sqlx.DB
	BasicRepository
}

func NewTransactionCancellationRepository(db *sqlx.DB) TransactionCancellationRepository {
	return TransactionCancellationRepository{
		DB: db,
		BasicRepository: BasicRepository{
			DB:        db,
			TableName: "transaction_cancellations",
		},
	}
}
