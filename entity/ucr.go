package entity

import "database/sql"

type Ucr struct {
	UcrName                 sql.NullString `db:"ucr_name"`
	UcrAddress              sql.NullString `db:"ucr_address"`
	UcrAddressCity          sql.NullString `db:"ucr_address_city"`
	UcrAddressCountry       sql.NullString `db:"ucr_address_country"`
	UcrIdType               sql.NullString `db:"ucr_id_type"`
	UcrOrganisationBic      sql.NullString `db:"ucr_organisation_bic"`
	UcrPrivateBirth         sql.NullString `db:"ucr_private_birth"`
	UcrPrivateBirthProvince sql.NullString `db:"ucr_private_birth_province"`
	UcrPrivateBirthCity     sql.NullString `db:"ucr_private_birth_city"`
	UcrPrivateBirthCountry  sql.NullString `db:"ucr_private_birth_country"`
	UcrIdentification       sql.NullString `db:"ucr_identification"`
	UcrSchemeCode           sql.NullString `db:"ucr_scheme_code"`
	UcrSchemeProprietary    sql.NullString `db:"ucr_scheme_proprietary"`
	UcrIssuer               sql.NullString `db:"ucr_issuer"`
}
