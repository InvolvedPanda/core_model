package entity

import "database/sql"

type Udr struct {
	UdrName                 sql.NullString `db:"udr_name"`
	UdrAddress              sql.NullString `db:"udr_address"`
	UdrAddressCity          sql.NullString `db:"udr_address_city"`
	UdrAddressCountry       sql.NullString `db:"udr_address_country"`
	UdrIdType               sql.NullString `db:"udr_id_type"`
	UdrOrganisationBic      sql.NullString `db:"udr_organisation_bic"`
	UdrPrivateBirth         sql.NullString `db:"udr_private_birth"`
	UdrPrivateBirthProvince sql.NullString `db:"udr_private_birth_province"`
	UdrPrivateBirthCity     sql.NullString `db:"udr_private_birth_city"`
	UdrPrivateBirthCountry  sql.NullString `db:"udr_private_birth_country"`
	UdrIdentification       sql.NullString `db:"udr_identification"`
	UdrSchemeCode           sql.NullString `db:"udr_scheme_code"`
	UdrSchemeProprietary    sql.NullString `db:"udr_scheme_proprietary"`
	UdrIssuer               sql.NullString `db:"udr_issuer"`
}
