package entity

import (
	"github.com/jmoiron/sqlx"
)

type BasicRepository struct {
	DB        *sqlx.DB
	TableName string
}

func (repo BasicRepository) GetAll(dest interface{}) error {
	return repo.DB.Select(dest, "SELECT * FROM "+repo.TableName)
}

func (repo BasicRepository) GetById(id int64, dest interface{}) error {
	return repo.DB.Get(dest, "SELECT * FROM "+repo.TableName+" WHERE id=$1", id)
}

func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}