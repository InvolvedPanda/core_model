package utils

import (
	"bitbucket.org/InvolvedPanda/core_model/entity"
	"github.com/jmoiron/sqlx"
	"github.com/pechenini/errors"
	"github.com/uniplaces/carbon"
	"time"
)

type Calendar struct {
	CalendarRepo    entity.SystemCalendarRepository
	DomainValueRepo entity.DomainValueRepository
}

func NewCalendar(db *sqlx.DB) Calendar {
	return Calendar{
		CalendarRepo:    entity.NewSystemCalendarRepository(db),
		DomainValueRepo: entity.NewDomainValueRepository(db),
	}
}

func (c Calendar) CreateInternalSettlementDate() time.Time {
	startOfDay := carbon.Now().StartOfDay()
	return startOfDay.Time
}

func (c Calendar) CreateSepaSettlementDate(isStep2 bool) (time.Time, error) {
	startOfDay := carbon.Now().StartOfDay()
	var endOfDayDw entity.DomainValue
	var err error
	if isStep2 {
		endOfDayDw, err = c.DomainValueRepo.GetEndOfDayStep2()
		if err != nil {
			return time.Now(), errors.Wrap(err)
		}
	} else {
		endOfDayDw, err = c.DomainValueRepo.GetEndOfDay()
		if err != nil {
			return time.Now(), errors.Wrap(err)
		}
	}

	enfOfDayTime := carbon.Now()
	err = enfOfDayTime.SetTimeFromTimeString(endOfDayDw.HighValue)
	if err != nil {
		return time.Now(), errors.Wrap(err)
	}

	currentTime := carbon.Now()
	businessDay, err := c.CalendarRepo.GetBusinessDay(currentTime.Time)
	if err != nil {
		return startOfDay.Time, err
	}

	if businessDay == nil || currentTime.Format("15:04:05") > enfOfDayTime.Format("15:04:05") {
		nextBDay, err := c.CalendarRepo.GetNextBusinessDay(currentTime.Time)
		if err != nil {
			return startOfDay.Time, err
		}
		nextBDayCarbonTime, err := carbon.CreateFromFormat(carbon.DefaultFormat, nextBDay.BusinessDayEnd.Time.Format(carbon.DefaultFormat), "")
		if err != nil {
			return startOfDay.Time, errors.Wrap(err)
		}
		return nextBDayCarbonTime.StartOfDay().Time, nil
	}

	return startOfDay.Time, nil
}
