module bitbucket.org/InvolvedPanda/core_model

go 1.13

require (
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/pechenini/errors v1.3.0
	github.com/uniplaces/carbon v0.1.6
)
